//
//  ViewController.m
//  Lab
//
//  Created by user on 5/30/17.
//  Copyright © 2017 user. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, retain) NSMutableArray *matrix;
@property (nonatomic, retain) NSMutableArray *matrixWithOnlyRightWay;
@property (nonatomic) int size;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createMatrixWithSize:30];
    [self createRightWay:self.matrix];
    [self createWrongWay:self.matrix];
    [self printMatrix:self.matrix];
}


# pragma mark - create matrix

- (void) createMatrixWithSize: (int) size {
    self.matrix = [[NSMutableArray alloc] init];
    for (int y = 0; y < size; y++){
        NSMutableArray *lineArray = [NSMutableArray array];
        for (int x = 0; x < size; x++){
            [lineArray addObject:@"*"];
        }
        [self.matrix addObject:lineArray];
    }
    self.size = size;
}


# pragma mark - create true direction

-(NSMutableArray *) createRightWay: (NSMutableArray *) matr {
    int lastPosition = 0;
    for (int y = 0; y < self.size - 1; y +=2 ){
        int random_x = arc4random_uniform(self.size - 2) + 1;
        if ( lastPosition != 0){
            if (lastPosition < random_x){
                for (int a = 0; a < random_x - lastPosition; a++){
                    matr[lastPosition + a][y] = @" ";
                }
            }
            else {
                for (int a = 0; a < lastPosition - random_x; a++){
                    matr[lastPosition - a][y] = @" ";
                }
            }
        }
        matr[random_x][y] = @" ";
        matr[random_x][y+1] = @" ";
        lastPosition = random_x;
    }
    if (self.size % 2 != 0) {
        matr[lastPosition][self.size-1] = @" ";
    }
    self.matrixWithOnlyRightWay = [[NSMutableArray alloc] initWithArray:matr copyItems:YES];
    return matr;
}


# pragma mark - create wrong direction

-(NSMutableArray *) createWrongWay: (NSMutableArray *) matr {
    for (int a = 0; a < self.size  ; a++){
        int x = arc4random_uniform(self.size - 2) + 1;
        int y = arc4random_uniform(self.size - 2) + 1;
        if  ([matr [x][y]  isEqual: @"*" ]  &&  ![self isBounds: x y: y]) {
            do{
                matr[x][y] = @" ";
                int random_direction = arc4random_uniform(3);
                switch (random_direction) {
                    case 0:
                        //go down
                        y += 1;
                        break;
                    case 1:
                        //go left
                        x -=  1;
                        break;
                    case 2:
                        //go up
                        y -= 1;
                        break;
                    case 3:
                        //go right
                        x += 1;
                        break;
                    default:
                        break;
                }
            }
            while ( ![self isBounds: x y: y] && ![self.matrixWithOnlyRightWay [x][y] isEqual: @" "] );
        }
    }
    return matr;
}


# pragma mark - print matrix

-(void) printMatrix: (NSArray *	) matr {
    NSMutableString *line = [NSMutableString stringWithString:@""];
    for (int y = 0; y < self.size; y++){
        for (int x = 0; x < self.size; x++){
            [line appendString: matr [x][y]];
        }
        NSLog(@"%@",line);
        [line setString:@""];
    }
}


# pragma mark - check bounds

- (BOOL) isBounds: (int) x y: (int) y {
    if (x == 0 || x == self.size - 1  || y == 0 || y == self.size - 1 ){
        return true;
    }
    return false;
}

@end
